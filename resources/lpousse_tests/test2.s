.name "test"
.comment "My life is potato(-testing)"

l2:
		sti r1, %:live, %1
		sti r1, %:flive, %1
		ld %65, r16
		ld %9, r3
		and r1, %0, r1
flive:	live %1
for:	fork %:flive
		aff r16
		aff r16
		aff r16
		aff r16
		aff r16
		sti r3, %:for, %0

live:
		live %1
		zjmp %:live

#include "unistd.h"
#include <sys/types.h>
#include <sys/uio.h>
#include "fcntl.h"
#include "stdlib.h"
#include "stdio.h"

void ft_putchar_fd(char c, int fd)
{
	write(fd, &c, 1);
}

int main (int ac, char ** av)
{
	int		fd;
	int		offset;
	int		ret;
	char	value;
	char	buf[4];

	offset = 4 + 132 + 4 + 2052 + 1;
	value = atoi(av[2]);
	if (ac != 3)
		return (0);
	fd = open(av[1], O_WRONLY);
	if (fd < 0)
		return (-1);
	if ((ret = lseek(fd, offset, SEEK_SET)) < 0)
		return (-1);
	ft_putchar_fd(value, fd);
	close(fd);
	write(1, "success\n", 8);
	return (1);
}

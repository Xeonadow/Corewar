.name "test"
.comment "My life is potato(-testing)"

l2:
	sti r1, %:l4, %1
	and r1, %0, r1
	fork %:l3
	fork %:l3

l3:
	fork %:l4

l4:
	live %1
	zjmp %:l4
